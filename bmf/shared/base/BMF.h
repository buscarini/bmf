

#import "BMFTypes.h"


#import "BMFBase.h"

#import <BMF/BMFArrayProxy.h>

#define BMFLocalized(string,comment) NSLocalizedStringFromTableInBundle(string,@"BMFLocalizable",[BMFBase sharedInstance].bundle,comment)

#pragma mark Utils
#import "BMFAutolayoutUtils.h"
#import <BMF/BMFUtils.h>
#import <BMF/BMFKeyboardManager.h>

#pragma mark Protocols
#import "BMFParserProtocol.h"
#import "BMFTaskProtocol.h"

#pragma mark Categories
#import "NSBundle+BMF.h"
#import "NSArray+BMF.h"
#import "NSSet+BMF.h"
#import "NSMutableArray+BMF.h"
#import "NSMutableDictionary+BMF.h"
#import "NSMutableSet+BMF.h"

#import "BMFDataReadProtocol.h"
#import "BMFDataStoreProtocol.h"
#import "BMFValueProtocol.h"

#import "BMFUserMessagesPresenterProtocol.h"
#import "BMFObjectControllerProtocol.h"

#import "BMFBehaviorsViewControllerProtocol.h"


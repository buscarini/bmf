//
//  BMFProxy.m
//  BMF
//
//  Created by Jose Manuel Sánchez Peñarroja on 10/03/14.
//  Copyright (c) 2014 José Manuel Sánchez. All rights reserved.
//

#import "BMFArrayProxy.h"

#import "BMF.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

@interface BMFArrayProxy()

@property (nonatomic, strong) NSMutableSet *destinationObjects;
@property (nonatomic, strong) RACSubject *destinationsSignal;
@property (nonatomic, strong) dispatch_queue_t serialQueue;

@end

@implementation BMFArrayProxy

- (instancetype)init {
    if (self) {
		_serialQueue = dispatch_queue_create("Proxy queue", DISPATCH_QUEUE_SERIAL);
		
        _destinationObjects = [NSMutableSet set];
		_destinationsSignal = [RACSubject subject];
    }
    return self;
}
+ (instancetype)new {
	return [[BMFArrayProxy alloc] init];
}

- (void) dealloc {
	[(RACSubject *)self.destinationsSignal sendCompleted];
	[self removeAllDestinationObjects];
}

- (void) setObject:(id)object {
	dispatch_async(self.serialQueue, ^{
		for (id obj in self.destinationObjects) {
			if ([obj respondsToSelector:@selector(setObject:)]) {
				dispatch_async(dispatch_get_main_queue(), ^{
					[obj setObject:object];
				});
			}
		}
	});
	
	_object = object;
}

- (void) addDestinationObject:(id) object {
	
	BMFAssertReturn(object);
	BMFAssertReturn(object!=self.object);
		
	if (self.object && [object respondsToSelector:@selector(setObject:)]) {
		[object setObject:self.object];
	}

	__block NSSet *destinationsCopy = nil;
	dispatch_sync(self.serialQueue, ^{
		[self.destinationObjects addObject:object];
		destinationsCopy = [self.destinationObjects copy];
	});
	
	[(RACSubject *)self.destinationsSignal sendNext:destinationsCopy];
}

- (void) removeDestinationObject:(id) object {

	__block NSSet *destinationsCopy = nil;
	dispatch_sync(self.serialQueue, ^{
		if (object) [self.destinationObjects removeObject:object];
		destinationsCopy = [self.destinationObjects copy];
	});
	
	[(RACSubject *)self.destinationsSignal sendNext:destinationsCopy];
}

- (void) removeAllDestinationObjects {
	dispatch_sync(self.serialQueue, ^{
		[self.destinationObjects removeAllObjects];
	});

	[(RACSubject *)self.destinationsSignal sendNext:[NSSet set]];
}

- (BOOL)respondsToSelector:(SEL)aSelector {
//#warning Check if this works ok
//	return YES;
	
	__block BOOL result = NO;
	dispatch_sync(self.serialQueue, ^{
		for (id obj in self.destinationObjects) {
			if ([obj respondsToSelector:aSelector]) {
				result = YES;
				return;
			}
		}
	});
	
    return result;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel {
	__block NSMethodSignature *sig = nil;

	dispatch_sync(self.serialQueue, ^{
		for(id obj in self.destinationObjects) {
			sig = [obj methodSignatureForSelector:sel];
			if (sig) return;
		}
	});

	return sig;
}

- (void)forwardInvocation:(NSInvocation *)inv {
	__block NSArray *objects = nil;
	dispatch_sync(self.serialQueue, ^{
		objects = self.destinationObjects.allObjects;
	});
	
	for(id obj in objects) {
		if ([obj respondsToSelector:inv.selector]) {
			[inv invokeWithTarget:obj];
		}
	}		
}

@end

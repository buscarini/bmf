//
//  NSArray+BMF.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 17/12/14.
//
//

#import "NSArray+BMF.h"

@implementation NSArray (BMF)

- (void) BMF_forEach:(BMFItemBlock)block {
	for (id item in self) {
		block(item);
	}
}

- (NSArray *) BMF_map:(BMFMapBlock)block {
	NSMutableArray *results = [NSMutableArray array];
	for (id object in self) {
		id result = block(object);
		if (result) [results addObject:result];
	}
	return [results copy];
}

- (NSArray *) BMF_filter:(BMFFilterBlock)block {
	NSMutableArray *results = [NSMutableArray array];
	for (id object in self) {
		if (block(object)) [results addObject:object];
	}
	return [results copy];
}

@end

//
//  NSArray+BMF.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 17/12/14.
//
//

#import <Foundation/Foundation.h>

#import "BMFTypes.h"

@interface NSArray (BMF)

- (void) BMF_forEach:(BMFItemBlock)block;
- (NSArray *) BMF_map:(BMFMapBlock)block;
- (NSArray *) BMF_filter:(BMFFilterBlock)block;

@end

//
//  NSSet+BMF.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 12/1/15.
//
//

#import "NSSet+BMF.h"

@implementation NSSet (BMF)

- (void) BMF_forEach:(BMFItemBlock)block {
	for (id item in self) {
		block(item);
	}
}

- (NSSet *) BMF_map:(BMFMapBlock)block {
	NSMutableArray *results = [NSMutableArray array];
	for (id object in self) {
		id result = block(object);
		if (result) [results addObject:result];
	}
	return [NSSet setWithArray:results];
}

- (NSSet *) BMF_filter:(BMFFilterBlock)block {
	NSMutableArray *results = [NSMutableArray array];
	for (id object in self) {
		if (block(object)) [results addObject:object];
	}
	return [NSSet setWithArray:results];
}

@end

//
//  BMFIXFont+BMF.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 30/06/14.
//
//

#import "BMFIXFont+BMF.h"

@implementation BMFIXFont (BMF)

- (CTFontRef) BMF_ctFont {
	return CTFontCreateWithName((CFStringRef)self.fontDescriptor.postscriptName, self.pointSize, NULL);
}

@end

//
//  BMFBinding.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 20/11/14.
//
//

#import <Foundation/Foundation.h>

#import "BMFBindingProtocol.h"
#import "BMFAspect.h"

@interface BMFBinding : BMFAspect <BMFBindingProtocol>

@property (nonatomic, weak) IBOutlet id model;
@property (nonatomic, weak) IBOutlet UIControl *control;

@property (nonatomic, strong) NSString *propertyName;

@end

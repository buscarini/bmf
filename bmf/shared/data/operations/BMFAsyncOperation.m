//
//  BMFAsyncOperation.m
//  BMF
//
//  Created by Jose Manuel Sánchez Peñarroja on 08/01/14.
//  Copyright (c) 2014 José Manuel Sánchez. All rights reserved.
//

#import "BMFAsyncOperation.h"

@implementation BMFAsyncOperation {
	BOOL _isExecuting;
	BOOL _isFinished;
}

@synthesize cancelled = _cancelled;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.progress = [BMFProgress new];
    }
    return self;
}

- (void) start {
	
	if (![NSThread isMainThread]) {
		[self performSelectorOnMainThread:@selector(start) withObject:nil waitUntilDone:NO];
		return;
	}
	
	[self willChangeValueForKey:@"isExecuting"];
	[self willChangeValueForKey:@"isFinished"];
	_isExecuting = YES;
	_isFinished = NO;
	[self didChangeValueForKey:@"isExecuting"];
	[self didChangeValueForKey:@"isFinished"];

	//if (self.progress.children.count==0) [self.progress start:@"com.bmf.AsyncOperation"];
	
	[self performStart];
}

- (void) cancel {
	[self performCancel];
	
	//if (self.progress.children.count==0) [self.progress stop:nil];
	
	[self finished];
	
	[self willChangeValueForKey:@"isCancelled"];
	_cancelled = YES;
	[self didChangeValueForKey:@"isCancelled"];

}

- (void)finished {
	[self willChangeValueForKey:@"isExecuting"];
	_isExecuting = NO;
	[self didChangeValueForKey:@"isExecuting"];
	
	[self willChangeValueForKey:@"isFinished"];
	_isFinished = YES;
	[self didChangeValueForKey:@"isFinished"];
	
	
	//[self.progress stop:nil];
}

- (BOOL) isCancelled {
	return _cancelled;
}

- (BOOL) isExecuting {
	return _isExecuting;
}

- (BOOL) isConcurrent {
	return YES;
}

- (BOOL) isFinished {
	return _isFinished;
}

- (void) performStart {}
- (void) performCancel {}

- (void) dealloc {
	[self performCancel];
}

@end

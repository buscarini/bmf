//
//  TNProgress.m
//  DataSources
//
//  Created by José Manuel Sánchez on 12/11/13.
//  Copyright (c) 2013 treenovum. All rights reserved.
//

#import "BMFProgress.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/RACEXTScope.h>

#import "BMF.h"

#import "BMFMutableWeakArray.h"

#import "BMFAverageTime.h"

@interface BMFProgress()

@property (nonatomic,assign) BOOL running;
@property (nonatomic, strong) BMFMutableWeakArray *children;
@property (nonatomic, copy) BMFActionBlock progressChangedBlock;

@end

@implementation BMFProgress {
	dispatch_queue_t serialQueue;
}

@synthesize running = _running;
@synthesize estimatedTime = _estimatedTime;
@synthesize totalUnitCount = _totalUnitCount;
@synthesize completedUnitCount = _completedUnitCount;
@synthesize progressMessage = _progressMessage;
@synthesize failedError = _failedError;

- (id)init {
    self = [super init];
    if (self) {
		serialQueue = dispatch_queue_create("BMFProgress queue", DISPATCH_QUEUE_SERIAL);
		
		self.totalUnitCount = 1;
		self.estimatedTime = 1; // 1 second by default
    }
    return self;
}

- (BMFMutableWeakArray *) children {
	__block BMFMutableWeakArray *result = nil;
	dispatch_sync(serialQueue, ^{
		if (!_children) _children = [BMFMutableWeakArray new];
		result = _children;
	});
	
	return result;
}

- (void) updateValuesFromChildren {
	__block int64_t total = 0;
	__block int64_t completed = 0;
	__block int64_t estimated = 0;

	[self willChangeValueForKey:@"progressMessage"];
	[self willChangeValueForKey:@"running"];
	
	dispatch_sync(serialQueue, ^{
		_progressMessage = nil;
		_running = NO;
		
		for (BMFProgress *child in _children) {
			if (child.running) {
				_running = YES;
				
				NSString *childMessage = child.progressMessage;
				if (childMessage.length>0 && !_progressMessage) {
					_progressMessage = childMessage;
				}
			}
			
			int64_t childEstimated = child.estimatedTime;
			CGFloat childTotal = childEstimated*1000;
			
			total += childTotal;
			completed += child.fractionCompleted*childTotal;
			estimated += childEstimated;
		}
		
		_totalUnitCount = total;
		_completedUnitCount = completed;
		_estimatedTime = estimated;

	});
	
	[self didChangeValueForKey:@"running"];
	[self didChangeValueForKey:@"progressMessage"];
		
//	DDLogInfo(@"update Progress: %@ %@",self,self.changedBlock);
	if (self.progressChangedBlock) self.progressChangedBlock(self);
	if (self.changedBlock) self.changedBlock(self);
}

- (int64_t) totalUnitCount {
	__block int64_t result = 0;
	
	dispatch_sync(serialQueue, ^{
		result = _totalUnitCount;
	});
	
	// Don't allow a total unit count less than 1
	return MAX(result,1);
}

- (void) setTotalUnitCount:(int64_t)totalUnitCount {
	dispatch_sync(serialQueue, ^{
		_totalUnitCount = totalUnitCount;
	});

	if (self.key.length>0) [BMFAverageTime setEffort:totalUnitCount forKey:self.key sender:self];
}

- (CGFloat) fractionCompleted {
	__block CGFloat result = 0;
	dispatch_sync(serialQueue, ^{
		if (_totalUnitCount==0) result = 0;
		else {
			result = _completedUnitCount/(CGFloat)_totalUnitCount;	
		}
	});
	
	// Result should be between 0 and 1
	return MAX(MIN(result,1),0);
}

- (void) setProgressMessage:(NSString *)progressMessage {
	dispatch_sync(serialQueue, ^{
		_progressMessage = [progressMessage copy];
	});
	
	if (self.progressChangedBlock) self.progressChangedBlock(self);
	
	if (self.changedBlock) self.changedBlock(self);
}

- (NSString *) progressMessage {
	__block NSString *result = nil;
	dispatch_sync(serialQueue, ^{
		result = [_progressMessage copy];
	});

	return result;
}

- (void) clear {
	self.completedUnitCount = 0;
	self.progressMessage = nil;
	self.failedError = nil;
}

- (void) reset {
	dispatch_sync(serialQueue, ^{
		for (BMFProgress *child in _children) {
			child.progressChangedBlock = nil;
		}

		[_children removeAllObjects];
	});

	[self clear];
}

- (void) setKey:(NSString *)key {
	dispatch_sync(serialQueue, ^{
		_key = key;
	});
	
	if (key.length>0) {
		self.estimatedTime = [BMFAverageTime averageTime:key effort:self.totalUnitCount];
	}
}

- (BOOL) running {
	__block BOOL result = NO;
	dispatch_sync(serialQueue, ^{
		result = _running;
	});
	return result;
}

- (void) setRunning:(BOOL)running {
	dispatch_sync(serialQueue, ^{
		_running = running;
	});
	
	if (self.progressChangedBlock) self.progressChangedBlock(self);

	if (self.changedBlock) self.changedBlock(self);
}

- (NSTimeInterval) estimatedTime {
	__block NSTimeInterval result = 0;
	dispatch_sync(serialQueue, ^{
		result = _estimatedTime;
	});
	
	// Don't allow 0 as estimated time
	return MAX(result,0.000001);
}

- (void) setEstimatedTime:(NSTimeInterval)estimatedTime {
	dispatch_sync(serialQueue, ^{
		_estimatedTime = estimatedTime;
	});

	if (self.progressChangedBlock) self.progressChangedBlock(self);
	if (self.changedBlock) self.changedBlock(self);
}

- (int64_t) completedUnitCount {
	__block int64_t result = 0;
	dispatch_sync(serialQueue, ^{
		result = _completedUnitCount;
	});
	
	return result;
}

- (void) setCompletedUnitCount:(int64_t)completedUnitCount {
	dispatch_sync(serialQueue, ^{
		_completedUnitCount = completedUnitCount;
	});
	
	if (self.progressChangedBlock) self.progressChangedBlock(self);
	if (self.changedBlock) self.changedBlock(self);
}

- (NSError *) failedError {
	__block NSError *result = nil;
	dispatch_sync(serialQueue, ^{
		result = _failedError;
	});
	
	return result;
}

- (void) setFailedError:(NSError *)failedError {
	[self willChangeValueForKey:@"failedError"];
	[self willChangeValueForKey:@"progressMessage"];
	dispatch_sync(serialQueue, ^{
		_failedError = failedError;
		_progressMessage = [failedError localizedDescription];
	});
	[self didChangeValueForKey:@"progressMessage"];
	[self didChangeValueForKey:@"failedError"];
	
	if (self.progressChangedBlock) self.progressChangedBlock(self);
	if (self.changedBlock) self.changedBlock(self);
}

- (void) addChild:(BMFProgress *) child {

	if (!child) {
		return;	
	}
	
	BMFMutableWeakArray *children = self.children;
	dispatch_sync(serialQueue, ^{
		[children addObject:child];
	});
	
	child.progressChangedBlock = ^(BMFProgress *child) {
		[self updateValuesFromChildren];
	};
}

- (void) removeChild:(BMFProgress *) child {
	if (!child) {
		return;
	}
	
	BMFMutableWeakArray *children = self.children;
	dispatch_sync(serialQueue, ^{
		[children removeObject:child];
	});
	
	child.progressChangedBlock = nil;
}

- (void) start:(NSString *) key {
	self.key = key;
	[self start];
}

- (void) start {
	
	BMFAssertReturn(self.key.length>0);
	BMFAssertReturn(self.children.count==0);

	[self clear];
	
	[BMFAverageTime startTime:self.key effort:self.totalUnitCount sender:self];
	
	self.running = YES;
}

- (void) stop: (NSError *) error {
	BMFAssertReturn(self.children.count==0);

	if (self.key && self.running) {
		if (error) [BMFAverageTime cancelTime:self.key];
		else [BMFAverageTime stopTime:self.key sender:self];
		
		self.key = nil;
	}
	else {
		/// If we are running without a key something is wrong. We should always be started with a key
		BMFAssertReturn(!self.running);
	}
	
	self.running = NO;
	self.completedUnitCount = self.totalUnitCount;
	self.failedError = error;
}

- (void) dealloc {
	[self clear];
}

- (NSString *) description {
	return [NSString stringWithFormat:@"<%@: %p, %@>",[self class],self,@{
																		  @"running" : @(self.running),
																		  @"total" : @(self.totalUnitCount),
																		  @"completed" : @(self.completedUnitCount),
																		  @"estimated time" : @(self.estimatedTime),
																		  @"message" : [NSString BMF_nonNilString:self.progressMessage],
																		  @"error" : [BMFUtils objectOrNull:self.failedError]
																		  }];
}

@end

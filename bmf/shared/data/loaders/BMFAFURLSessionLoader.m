//
//  TNAFNetworkingLoader.m
//  DataSources
//
//  Created by José Manuel Sánchez on 22/10/13.
//  Copyright (c) 2013 treenovum. All rights reserved.
//

#import "BMFAFURLSessionLoader.h"

#import "BMF.h"

#import "BMFUtils.h"

#import <AFNetworking/AFNetworking.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <malloc/malloc.h>

static AFHTTPSessionManager *sessionManager = nil;

@interface BMFAFURLSessionLoader() <NSURLSessionDelegate,NSURLSessionTaskDelegate,NSURLSessionDataDelegate>
	@property (nonatomic, strong) NSURL *finalUrl;
@end

@implementation BMFAFURLSessionLoader {
	NSURLSessionDataTask *dataTask;
}

@synthesize progress = _progress;

- (id)init
{
    self = [super init];
    if (self) {
        self.configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
		_progress = [[BMFProgress alloc] init];
		_method = @"GET";
		
		_responseSerializer = [[AFHTTPResponseSerializer alloc] init];
		
//		_cache = [[NSCache alloc] init];
		
		#if TARGET_OS_IPHONE
		[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil] subscribeNext:^(id x) {
			[sessionManager invalidateSessionCancelingTasks:NO];
			sessionManager = nil;
		}];
		#endif
    }
    return self;
}

- (void) cancel {
	[dataTask cancel];
	[self.progress stop:nil];
}

- (void) setUrl:(NSURL *)url {
	_url = url;
	if (_url) [self.progress setKey:_url.absoluteString];
}

//- (void) setAcceptableContentTypes:(NSSet *)acceptableContentTypes {
//	_acceptableContentTypes = acceptableContentTypes;
//	self.responseSerializer.acceptableContentTypes = acceptableContentTypes;
//}

- (BOOL) callBlocksForUrl:(NSURL *)url result:(id)result error:(NSError *)error {
	if (!self.cache) return NO;
	
	NSArray *blocks = [self.cache objectForKey:self.url];
	if (![blocks isKindOfClass:[NSArray class]]) return NO;
	
	for (BMFCompletionBlock block in blocks) {
		block(result,error);
	}
	
	// Blocks should only be called once
	[self.cache removeObjectForKey:self.url];

	return YES;
}

- (void) load:(BMFCompletionBlock) completionBlock {
	
	BMFAssertReturn(self.url);
	BMFAssertReturn(completionBlock);

	self.finalUrl = self.url;
	
	[_progress start:self.url.absoluteString];
	
	_progress.progressMessage = BMFLocalized(@"Loading data", nil);
	
	if (self.cache) {
		id responseObject = [self.cache objectForKey:self.url];
		if (!responseObject) {
			responseObject = [NSMutableArray array];
		}
		
		if ([responseObject isKindOfClass:[NSMutableArray class]]) {
			[responseObject addObject:completionBlock];
			[self.cache setObject:responseObject forKey:self.url];
		}
		else {
//				[_progress start:[NSString stringWithFormat:@"%@_cache",self.url.absoluteString]];
			DDLogInfo(@"Loader returning cached data");
			[_progress stop:nil];
			completionBlock(responseObject,nil);
			return;
		}
	}
	
	
	DDLogDebug(@"Load url: %@ params: %@",self.url.absoluteString,self.parameters);
	
	/*AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:self.configuration];
	 
	 [manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition (NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential * __autoreleasing *credential) {
	 return NSURLSessionAuthChallengePerformDefaultHandling;
	 }];
	 
	 AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
	 serializer.acceptableContentTypes = self.acceptableContentTypes;
	 manager.responseSerializer = serializer;*/
	if (!sessionManager) sessionManager = [AFHTTPSessionManager manager];
	
	[sessionManager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition (NSURLSession *session, NSURLAuthenticationChallenge *challenge, NSURLCredential * __autoreleasing *credential) {
		return NSURLSessionAuthChallengePerformDefaultHandling;
	}];
	
	if (self.requestSerializer) sessionManager.requestSerializer = self.requestSerializer;
	if (self.responseSerializer) sessionManager.responseSerializer = self.responseSerializer;

	if (self.acceptableContentTypes) self.responseSerializer.acceptableContentTypes = self.acceptableContentTypes;

//		AFHTTPResponseSerializer *serializer = [AFHTTPResponseSerializer serializer];
//		serializer.acceptableContentTypes = self.acceptableContentTypes;
//		sessionManager.responseSerializer = serializer;
	
	//	NSURLRequest *request = [NSURLRequest requestWithURL:finalUrl];
	NSError *error = nil;
	NSMutableURLRequest *request = [sessionManager.requestSerializer requestWithMethod:self.method URLString:self.url.absoluteString parameters:self.parameters error:&error];
	if (error) {
		BMFLogErrorC(BMFLogNetworkContext,@"Error creating request: %@",error);
	}

	if (self.userAgent) {
		[request addValue:self.userAgent forHTTPHeaderField:@"User-Agent"];
	}
		
	if (self.httpBody) {
		NSData *bodyData = [self.httpBody dataUsingEncoding:NSUTF8StringEncoding];
		if (!bodyData) {
			BMFLogErrorC(BMFLogNetworkContext,@"Error, http body set but no data could be encoded: %@",self.httpBody);
		}
		[request setHTTPBody:bodyData];
	}

	if (self.contentType) {
		[request setValue:self.contentType forHTTPHeaderField:@"Content-Type" ];
	}
	
	dataTask = [sessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
		
		NSHTTPURLResponse *httpResponse = [NSHTTPURLResponse BMF_cast:response];
		
		BMFLogDebugC(BMFLogNetworkContext, @"Network response %d %@ %@ %@",httpResponse.statusCode,[NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode],response.URL,httpResponse.allHeaderFields);
		NSData *data = responseObject;
		NSString *responseBody = @"Body too big to log";
		
		if (data.length<2000) responseBody = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];

		BMFLogDebugC(BMFLogNetworkContext, @"Network response body %@",responseBody);

		if (error) {
			BMFLogErrorC(BMFLogNetworkContext,@"Request error: %@",error);
			
			if (![self callBlocksForUrl:self.url result:nil error:error]) {
				completionBlock(nil,error);
			}
		}
		else {
			if (![self callBlocksForUrl:self.url result:responseObject error:nil]) {
				completionBlock(responseObject,nil);
			}
			
			[self.cache setObject:responseObject forKey:self.url cost:malloc_size((__bridge const void *)(responseObject))];
		}
		
		[_progress stop:error];
	}];
	
	[sessionManager setTaskDidSendBodyDataBlock:^(NSURLSession *session, NSURLSessionTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
		int64_t sent = MAX(task.countOfBytesSent,0);
		int64_t received = MAX(task.countOfBytesReceived,0);

		_progress.completedUnitCount = sent+received;
	}];
	
	[sessionManager setDataTaskDidReceiveResponseBlock:^NSURLSessionResponseDisposition(NSURLSession *session, NSURLSessionDataTask *task, NSURLResponse *response) {
		int64_t expectedToSend = MAX(task.countOfBytesExpectedToSend,0);
		int64_t expectedToReceive = MAX(task.countOfBytesExpectedToReceive,0);
		_progress.totalUnitCount = expectedToSend + expectedToReceive;
		return NSURLSessionResponseAllow;
	}];
	
	[sessionManager setDataTaskDidReceiveDataBlock:^(NSURLSession *session, NSURLSessionDataTask *task, NSData *data) {
		int64_t sent = MAX(task.countOfBytesSent,0);
		int64_t received = MAX(task.countOfBytesReceived,0);

		_progress.completedUnitCount = sent+received;
	}];
	
	[sessionManager setTaskWillPerformHTTPRedirectionBlock:^NSURLRequest *(NSURLSession *session, NSURLSessionTask *task, NSURLResponse *response, NSURLRequest *request) {
		self.finalUrl = request.URL;
		return request;
	}];
	
	BMFLogDebugC(BMFLogNetworkContext, @"Network request %@ %@ %@ %@",self.method,request.URL,self.parameters,self.httpBody);
	
	[dataTask resume];
}

- (NSURL *) finalUrl {
	
	NSMutableString *finalUrlString = [self.url.absoluteString mutableCopy];
	if (self.parameters.allKeys.count>0) [finalUrlString appendString:@"?"];
	
	[self.parameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		NSString *escapedKey = [BMFUtils escapeURLString:key];
		NSString *escapedValue = [BMFUtils escapeURLString:obj];
		[finalUrlString appendString:escapedKey];
		[finalUrlString appendString:@"="];
		[finalUrlString appendString:escapedValue];
		[finalUrlString appendString:@"&"];
	}];
	
	return [NSURL URLWithString:finalUrlString];
}

@end

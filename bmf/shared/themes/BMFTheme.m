//
//  BMFThemeBase.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 18/06/14.
//
//

#import "BMFTheme.h"

#import "BMF.h"

static NSMutableArray *registeredList = nil;
static Class<BMFThemeProtocol> currentThemeInstance = nil;

@implementation BMFTheme

//+ (void) load {
//	if (!registeredList) {
//		registeredList = [NSMutableArray array];
//	}
//}

- (UIColor *) tintColor {
	BMFAbstractMethod();
	return nil;
}

- (NSString *) name {
	return @"default";
}

- (void) setupInitialAppearance {
	[[UIApplication sharedApplication] keyWindow].tintColor = self.tintColor;
}

- (void) setupView:(id) view {
	BMFAbstractMethod();
}

@end

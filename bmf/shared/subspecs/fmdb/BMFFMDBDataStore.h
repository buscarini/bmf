//
//  BMFFMDBDataStore.h
//  BMF
//
//  Created by Jose Manuel Sánchez Peñarroja on 05/03/14.
//  Copyright (c) 2014 José Manuel Sánchez. All rights reserved.
//

#import "BMFNode.h"

#import "BMFDataReadProtocol.h"
#import "BMFFMDBModelProtocol.h"

#import <FMDB/FMDatabase.h>

@class FMDatabase;

@interface BMFFMDBDataStore : BMFNode <BMFDataReadProtocol>

@property (nonatomic, strong) NSString *query;
@property (nonatomic, strong) NSArray *parameters;
@property (nonatomic, strong) id<BMFFMDBModelProtocol> modelClass;

@property (nonatomic, copy) NSString *sectionHeaderTitle;
@property (nonatomic, copy) NSString *sectionFooterTitle;

- (id)initWithQuery:(NSString *)query parameters:(NSArray *) parameters modelClass:(id<BMFFMDBModelProtocol>)modelClass;

@property (nonatomic, readonly) BOOL loaded;

- (BOOL) loadData:(BMFCompletionBlock) completionBlock;

@end

//
//  BMFUtils.h
//  Bonacasa
//
//  Created by Jose Manuel Sánchez Peñarroja on 20/01/14.
//  Copyright (c) 2014 treenovum. All rights reserved.
//

#import "BMFPlatform.h"

#import "BMFTypes.h"

#import "BMFLoaderProtocol.h"

@class RACSignal;

@interface BMFUtils : NSObject

+ (NSString *) currentLangCode;
+ (NSString *) currentUsedLangCode;
+ (NSString *) osVersion;
+ (NSString *) appVersion;
+ (NSString *) appBuild;
+ (NSString *) deviceOrientation;

+ (NSData *) makePDFFrom:(BMFIXView *) view;

+ (NSURL *)applicationSandboxStoresDirectory;
+ (NSString *)applicationDocumentsDirectory;
+ (NSString *)applicationCacheDirectory;

+ (BOOL) isRetinaDisplay;

+ (id) objectOrNull:(id) object;
+ (id) objectOrNil:(id) object;

+ (NSString *) escapeURLString: (NSString *)url;
+ (NSString *) unescapeURLString: (NSString *)url;

+ (NSString *) escapePathString: (NSString *)url;
+ (NSString *) unescapePathString: (NSString *)url;

+ (NSString *) htmlWithDefaultStyle:(NSString *) htmlString;
+ (NSString *) htmlWithDefaultStyle:(NSString *) htmlString fontSize:(NSInteger) fontSize;

+ (NSURL *) tmpFileUrl;

+ (NSInteger) randomInteger:(NSInteger) minIndex max:(NSInteger) maxIndex;
+ (double) randomDouble:(double) minIndex max:(double) maxIndex;
+ (NSString *) randomString:(NSInteger)length;
+ (NSString *) randomAlphaNumericString:(NSInteger)length;

+ (RACSignal *) webViewUserAgent:(NSURL *) url;

+ (NSString *) mimeTypeForExtension:(NSString *)extension;
+ (NSString *) mimeTypeForFileUrl:(NSURL *) fileUrl;

+ (NSData *) archiveImage:(BMFIXImage *) image;
+ (BMFIXImage *) unarchiveImage:(NSData *) imageData;

+ (CGRect) rectToFitRect:(CGRect) rect toRect:(CGRect) containerRect mode:(BMFContentMode)mode;

+ (CGRect) rectFromCenter:(CGPoint) center size:(CGSize) size;

/// Escape phone number for calling. This removes whitespaces, parenthesis and other symbols
+ (NSString *) escapePhoneString:(NSString *) phoneString;

/// Copies a file to a destination overwriting any existing file and creating intermediate paths
+ (BOOL) copyFileAtURL:(NSURL *) originUrl toUrl:(NSURL *) destinationUrl;
+ (BOOL) copyFileAtURL:(NSURL *) originUrl toUrl:(NSURL *) destinationUrl overwrite:(BOOL) overwrite createIntermediatePaths:(BOOL) createIntermediates;

/// Try to avoid using these methods. It is best to do things asynchronously. Using this easily produces deadlocks
+ (void) runSynchronously:(BMFAsyncBlock) block;
+ (void) runSynchronously:(BMFAsyncBlock) block timeout:(UInt64) timeout;

+ (void) performOncePerLaunch:(BMFBlock) block;

/// Calls the block once (remembering the state between launches). You can use resetTaskId to run it again
+ (void) performOnce:(BMFBlock) block taskId:(NSString *) taskId;

/// Runs this and stores the current app version. Next time it's called it will check if the app version has changed, and only if this is the case it will call the block. Can be reset by resetTaskId
+ (void) performOncePerVersion:(BMFBlock) block taskId:(NSString *) taskId;

/// This resets the task id so in the next call to performOnce it will call the block again
+ (void) resetTaskId:(NSString *)taskId;


#if TARGET_OS_IPHONE
+ (void) showNavigationBarLoading: (UIViewController *)vc;
+ (void) hideNavigationBarLoading: (UIViewController *)vc;

+ (NSString *) uniqueDeviceIdentifier;

+ (BOOL) markFileSkipBackup: (NSURL *)url;

+ (NSString*) sha1:(NSString*)input;
+ (NSString *) md5:(NSString *) input;

+ (UIImage *) imageWithView:(UIView *)view;
+ (UIImage *) cropImage:(UIImage *)image rect:(CGRect)rect;

+ (NSString *) stringForHTTPMethod:(BMFHTTPMethod)method;

/// Extracts the detail view controller from a vc that can be a container. For example, if the view controller is a navigation controller you will get the rootviewcontroller
+ (UIViewController *) extractDetailViewController:(UIViewController *) viewController;

+ (NSArray *) extractDetailViewControllers:(UIViewController *) viewController;

#endif

@end

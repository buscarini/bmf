//
//  UITableView+BMF.m
//  BMF
//
//  Created by Jose Manuel Sánchez Peñarroja on 29/01/14.
//  Copyright (c) 2014 José Manuel Sánchez. All rights reserved.
//

#import "UITableView+BMF.h"

@implementation UITableView (BMF)

- (void) BMF_showSeparatorsForEmptyCells {
	self.tableFooterView = nil;
}

- (void) BMF_hideSeparatorsForEmptyCells {
	UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
	self.tableFooterView = footer;
}

- (void) BMF_updateDelegate:(id)delegate {
	self.delegate = nil;
	self.delegate = delegate;
}

- (BOOL) BMF_isEmpty {
	id<UITableViewDataSource> dataSource = self.dataSource;
	if ([dataSource numberOfSectionsInTableView:self]==0) return YES;
	
	for (int i=0;i<[dataSource numberOfSectionsInTableView:self];i++) {
		NSInteger numRows = [dataSource tableView:self numberOfRowsInSection:i];
		if (numRows>0) return NO;
	}
	
	return YES;
}

- (BOOL) BMF_indexPathValid:(NSIndexPath *) indexPath {
	if (self.numberOfSections<=indexPath.section) return NO;
	if ([self numberOfRowsInSection:indexPath.section]<=indexPath.row) return NO;
	
	return YES;
}

- (void) BMF_reloadCellsAtIndexPaths:(NSArray *) indexPaths withAnimation:(UITableViewRowAnimation) updateAnimation {
	BOOL reloadView = NO;
	
	for (NSIndexPath *indexPath in indexPaths) {
		if (![self BMF_indexPathValid:indexPath]) {
			reloadView = YES;
			break;
		}
	}
	
	if (reloadView){
		[self reloadData];
	}
	else {
		[self reloadRowsAtIndexPaths:indexPaths withRowAnimation:updateAnimation];
	}
}

- (void) BMF_enumerateIndexPaths:(BMFObjectActionBlock) actionBlock {
	BMFAssertReturn(actionBlock);
	
	NSInteger numSections = [self numberOfSections];
	for (NSInteger i=0;i<numSections;i++) {
		NSInteger numItems = [self numberOfRowsInSection:i];
		for (NSInteger j=0;j<numItems;j++) {
			actionBlock([NSIndexPath indexPathForItem:j inSection:i]);
		}
	}
}

@end

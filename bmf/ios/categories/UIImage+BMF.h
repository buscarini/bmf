//
//  UIImage+BMF.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 18/08/14.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (BMF)

- (UIColor *) BMF_averageColor;

@end

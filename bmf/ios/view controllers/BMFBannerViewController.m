//
//  BMFBannerViewController.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 30/06/14.
//
//

#import "BMFBannerViewController.h"

#import "BMFCollectionViewDataSource.h"
#import "BMFTimerViewControllerBehavior.h"
#import "BMFEnumerateDataSourceAspect.h"

#import "BMFUpdateCollectionViewBehavior.h"
#import "BMFItemTapBlockBehavior.h"
#import "BMFScrollDragBlockBehavior.h"
#import "BMFScrollPageChangedBehavior.h"

#import "BMFBannerItemProtocol.h"

#import "BMF.h"

#import <ReactiveCocoa/RACEXTScope.h>

@interface BMFBannerViewController ()

@property (nonatomic, strong) BMFTimerViewControllerBehavior *timerBehavior;
@property (nonatomic, strong) NSLayoutConstraint *aspectRatioConstraint;

@end

@implementation BMFBannerViewController {
	id orientationObserver;
}

- (void) setSlideAutomatically:(BOOL)slideAutomatically {
	_slideAutomatically = slideAutomatically;
	
	self.timerBehavior.enabled = _slideAutomatically;
}

- (NSArray *) items {
	id<BMFDataReadProtocol> dataStore = self.dataSource.dataStore;
	return [dataStore allItems];
}

- (void) setItems:(NSArray *)items {
	
	id<BMFDataStoreProtocol> dataStore = [NSObject BMF_castObject:self.dataSource.dataStore withProtocol:@protocol(BMFDataStoreProtocol)];
	BMFAssertReturn(dataStore);
	
	[dataStore setItems:items];
	
	self.pageControl.numberOfPages = items.count;
	
	[self updateLayout];
	
	[self.collectionView reloadData];
}

- (void) viewDidLoad {
	[super viewDidLoad];
	
	BMFAssertReturn(self.cellClass);
	
	self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:[UICollectionViewFlowLayout new]];
	[self.view addSubview:self.collectionView];
	self.collectionView.pagingEnabled = YES;
	[BMFAutoLayoutUtils fill:self.view with:self.collectionView margin:0];
	
	id<BMFDataStoreProtocol, BMFDataReadProtocol> dataStore = (id)[[BMFBase sharedInstance].factory dataStoreWithParameter:@[] sender:nil];
	BMFAssertReturn([dataStore conformsToProtocol:@protocol(BMFDataStoreProtocol)] && [dataStore conformsToProtocol:@protocol(BMFDataReadProtocol)]);
	self.dataSource = (id)[[BMFBase sharedInstance].factory collectionViewDataSourceWithStore:dataStore cellClassOrNib:self.cellClass sender:self];
	
	self.scrollDirection = BMFBannerViewControllerScrollDirectionHorizontal;
	
	_slideDuration = 5;
	
	BMFEnumerateDataSourceAspect *enumerateAspect = [BMFEnumerateDataSourceAspect new];
	[self.dataSource BMF_addAspect:enumerateAspect];

	@weakify(self);
	self.timerBehavior = [[BMFTimerViewControllerBehavior alloc] initWithActionBlock:^(id sender) {
		[enumerateAspect action:sender completion:^(NSIndexPath *result, NSError *error) {
			@strongify(self);
			if (!error) {
				[self.collectionView selectItemAtIndexPath:result animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
			}
		}];
	} interval:_slideDuration];

	[self addBehavior:self.timerBehavior];
	
	[self addBehavior:[[BMFUpdateCollectionViewBehavior alloc] initWithView:self.collectionView]];
	
	BMFItemTapBlockBehavior *itemTapBehavior = [[BMFItemTapBlockBehavior alloc] initWithTapBlock:^(id item, NSIndexPath *indexPath) {
		if (self.selectItemBlock) self.selectItemBlock([self.dataSource itemAt:indexPath]);
	}];
	[self addBehavior:itemTapBehavior];
	
	BMFScrollDragBlockBehavior *dragBehavior = [[BMFScrollDragBlockBehavior alloc] initWithView:self.collectionView actionBlock:^(id sender) {
		self.timerBehavior.enabled = NO;
	}];
	
	[self addBehavior:dragBehavior];
	
	BMFScrollPageChangedBehavior *pageChangedBehavior = [[BMFScrollPageChangedBehavior alloc] initWithView:self.collectionView actionBlock:^(NSInteger pageIndex) {
		self.pageControl.currentPage = pageIndex;
		if (self.pageChangedBlock) self.pageChangedBlock(pageIndex);
	}];
	[self addBehavior:pageChangedBehavior];
	
	[self updateLayout];
}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.timerBehavior.enabled = self.slideAutomatically;
}

- (void) viewDidLayoutSubviews {
	[super viewDidLayoutSubviews];
	
	[self updateLayout];
}

- (void) updateLayout {
	UICollectionViewFlowLayout *flowLayout = [UICollectionViewFlowLayout BMF_cast:self.collectionView.collectionViewLayout];
	BMFAssertReturn(flowLayout);
	
	if (self.aspectRatioConstraint) [self.view removeConstraint:self.aspectRatioConstraint];
	
	if ([self.items.firstObject conformsToProtocol:@protocol(BMFBannerItemProtocol)]) {
		CGFloat	aspectRatio = [self.items.firstObject proportion];
		
		self.aspectRatioConstraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:aspectRatio constant:0];
		[self.view addConstraint:self.aspectRatioConstraint];
	}
	
	flowLayout.itemSize = self.view.bounds.size;
	
	flowLayout.minimumInteritemSpacing = 0;
	flowLayout.minimumLineSpacing = 0;
	flowLayout.sectionInset = UIEdgeInsetsZero;
	
	[self.view bringSubviewToFront:self.pageControl];
	
	UIView *loaderView = [UIView BMF_cast:self.loaderView];
	if (loaderView) [self.view bringSubviewToFront:loaderView];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
	
	UICollectionViewFlowLayout *layout =  [UICollectionViewFlowLayout BMF_cast:self.collectionView.collectionViewLayout];
	BMFAssertReturn(layout);
	layout.itemSize = self.view.bounds.size;
	[self.collectionView reloadData];
	dispatch_async(dispatch_get_main_queue(), ^{
		[self.collectionView selectItemAtIndexPath:[self.collectionView indexPathsForSelectedItems].firstObject animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
	});
}

- (void) setSlideDuration:(NSUInteger)slideDuration {
	_slideDuration = slideDuration;
	
	self.timerBehavior.interval = _slideDuration;
}

- (void) setScrollDirection:(BMFBannerViewControllerScrollDirection)scrollDirection {
	_scrollDirection = scrollDirection;
	
	UICollectionViewFlowLayout *flowLayout = [UICollectionViewFlowLayout BMF_cast:self.collectionView.collectionViewLayout];
	BMFAssertReturn(flowLayout);
	
	if (self.scrollDirection==BMFBannerViewControllerScrollDirectionHorizontal)	flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
	else flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
}

@end

//
//  BMFAlertViewUserMessagesPresenter.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 07/07/14.
//
//

#import "BMFAlertViewUserMessagesPresenter.h"

#import "BMFAlertView.h"


@implementation BMFAlertViewUserMessagesPresenter

- (void) showMessage:(NSString *) message kind:(BMFUserMessagesMessageKind)kind sender:(id)sender {
	BMFAlertView *alertView = [[BMFAlertView alloc] initWithTitle:nil message:message];
	[alertView show];
}

- (void) showMessage:(NSString *) title message:(NSString *) message kind:(BMFUserMessagesMessageKind)kind sender:(id)sender {
	BMFAlertView *alertView = [[BMFAlertView alloc] initWithTitle:title message:message];
	[alertView show];
}

@end

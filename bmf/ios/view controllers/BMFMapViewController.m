//
//  BMFMapViewController.m
//  DataSources
//
//  Created by José Manuel Sánchez on 28/10/13.
//  Copyright (c) 2013 treenovum. All rights reserved.
//

#import "BMFMapViewController.h"

#import "BMFMapAnnotation.h"
#import "BMFMapOverlay.h"
#import "BMFMapZoomer.h"
#import "BMFMapAnnotationFactory.h"

#import "BMFDataReadProtocol.h"

#import "MKMapView+BMF.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/RACEXTScope.h>

@interface BMFMapViewController ()

@end

@implementation BMFMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	@weakify(self);
	
	[[[RACObserve(self, dataSource) scanWithStart:nil reduce:^id(id running, id next) {
		@strongify(self);
		if (running) [self.BMF_proxy removeDestinationObject:running];
		return next;
	}] filter:^BOOL(id value) {
		return value!=nil;
	}] subscribeNext:^(id x) {
		@strongify(self);
		[self.BMF_proxy addDestinationObject:x];

		self.dataSource.view = self.mapView;
		
	}];

	RACSignal *mapViewSignal = RACObserve(self, mapView);
	
	RAC(self.dataSource,view) = mapViewSignal;
	
	[self.BMF_proxy.destinationsSignal subscribeNext:^(id x) {
		@strongify(self);
		[self.mapView BMF_updateDelegate:self.BMF_proxy];

	}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

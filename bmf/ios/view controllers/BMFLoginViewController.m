//
//  BMFLoginViewController.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 21/07/14.
//
//

#import "BMFLoginViewController.h"

#import <BMF/BMF.h>
#import <BMF/BMFProgress.h>

#import <BMF/BMFResponderChainBehavior.h>
#import <BMF/BMFAdjustObscuredTextFieldsBehavior.h>
#import <BMF/BMFBackgroundTapStopsEditingBehavior.h>

#import <ReactiveCocoa/RACEXTScope.h>

@interface BMFLoginViewController ()

@end

@implementation BMFLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

	[self fillDefaultValues];
		
//	@weakify(self);
//	RACSignal *buttonEnabledSignal = [RACSignal combineLatest:@[ userTextSignal, passTextSignal ] reduce:^(NSString *user,NSString *password){
//		@strongify(self);
//		return @(user.length>0 && password.length>0 && !self.loaderView.progress.running);
//	}];
	
	@weakify(self);
	self.loginButton.rac_command = [[RACCommand alloc] initWithEnabled:[self buttonEnabledSignal] signalBlock:^RACSignal *(id input) {
		return [RACSignal defer:^RACSignal *{
			@strongify(self);
			[self login:self];
			return [RACSignal empty];
		}];
	}];
	
//	RACSignal *userTextSignal = [self.userField rac_textSignal];
//	RACSignal *passTextSignal = [self.passwordField rac_textSignal];
	
	BMFResponderChainBehavior *textFieldsBehavior = [[BMFResponderChainBehavior alloc] initWithResponders:@[ self.userField,self.passwordField ]];
	textFieldsBehavior.lastResponderReturnKeyType = UIReturnKeySend;
	textFieldsBehavior.lastResponderActionBlock = ^(id sender) {
		[self login:self];
	};
	[self addBehavior:textFieldsBehavior];
	
	BMFAdjustObscuredTextFieldsBehavior *adjustTextFieldsBehavior = [[BMFAdjustObscuredTextFieldsBehavior alloc] initWithTextFields:@[ self.userField, self.passwordField ]];
	[self addBehavior:adjustTextFieldsBehavior];
	
	[self addBehavior:[BMFBackgroundTapStopsEditingBehavior new]];
}

- (RACSignal *) buttonEnabledSignal {
	RACSignal *userTextSignal = [self.userField rac_textSignal];
	RACSignal *passTextSignal = [self.passwordField rac_textSignal];

	@weakify(self);
	RACSignal *buttonEnabledSignal = [RACSignal combineLatest:@[ userTextSignal, passTextSignal ] reduce:^(NSString *user,NSString *password){
		@strongify(self);
		return @(user.length>0 && password.length>0 && !self.loaderView.progress.running);
	}];
	return buttonEnabledSignal;
}

- (void) fillDefaultValues {}

- (IBAction) login:(id) sender {
	id<BMFTaskProtocol> loginTask = [self loginTask:self.userField.text password:self.passwordField.text];
	[self.loaderView.progress addChild:loginTask.progress];
	[loginTask run:^(id result, NSError *error) {
		[self loginTaskFinished:result error:error];
	}];
}


- (id<BMFTaskProtocol>) loginTask:(NSString *)user password:(NSString *) password {
	BMFAbstractMethod();
	return nil;
}

- (void) loginTaskFinished:(id) result error:(NSError *) error {
	BMFAbstractMethod();
}

@end

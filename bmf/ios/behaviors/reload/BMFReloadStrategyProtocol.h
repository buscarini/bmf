//
//  BMFReloadStrategyProtocol.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 11/12/14.
//
//

#import <Foundation/Foundation.h>

#import "BMFTypes.h"

typedef NS_ENUM(NSUInteger, BMFReloadStrategyEvent) {
	BMFReloadStrategyEventViewLoaded,
	BMFReloadStrategyEventViewAppear,
	BMFReloadStrategyEventViewDisappear,
	BMFReloadStrategyEventForeground,
	BMFReloadStrategyEventDidBecomeActive,
	BMFReloadStrategyEventDataConnectionStatusChanged
};

@protocol BMFReloadStrategyProtocol <NSObject>

@property (nonatomic, copy) BMFActionBlock reloadBlock;

- (void) handleEvent:(BMFReloadStrategyEvent)event;

- (void) loaded:(BOOL) success;

@end

//
//  BMFReloadStrategy.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 11/12/14.
//
//

#import "BMFReloadStrategy.h"

#import "BMF.h"

@interface BMFReloadStrategy()

@property (nonatomic, strong) NSTimer *reloadTimer;
@property (nonatomic, assign) BOOL reloading;
@property (nonatomic, strong) NSDate *lastLoadDate;

@end

@implementation BMFReloadStrategy

- (instancetype) init {
	self = [super init];
	if (self) {
		_reloadInterval = -1;
		_miniumReloadInterval = 5;
	}
	return self;
}

- (void) dealloc {
	[self stopTimer];
}

#pragma mark Timer methods

- (void) startTimer {
	if (self.reloadInterval<=0) return;
	
	[self stopTimer];
	self.reloadTimer = [NSTimer scheduledTimerWithTimeInterval:self.reloadInterval target:self selector:@selector(timerUpdate:) userInfo:nil repeats:YES];
}

- (void) stopTimer {
	[self.reloadTimer invalidate];
	self.reloadTimer = nil;
}

- (void) timerUpdate:(NSTimer *) timer {
	[self reload];
}

- (void) reload {
	if (self.reloading) return;
	if (fabs([self.lastLoadDate timeIntervalSinceNow])<=self.miniumReloadInterval) return;
	
	if (self.reloadBlock) {
		self.reloadBlock(self);
		self.reloading = YES;
	}
}

#pragma mark Protocol methods

- (void) handleEvent:(BMFReloadStrategyEvent)event {
	if (event==BMFReloadStrategyEventViewDisappear) [self stopTimer];
}

- (void) loaded:(BOOL) success {
	self.reloading = NO;
	self.lastLoadDate = [NSDate date];
}


@end

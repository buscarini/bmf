//
//  BMFOnlyOnceReloadStrategy.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 11/12/14.
//
//

#import "BMFOnlyOnceOKReloadStrategy.h"

@interface BMFOnlyOnceOKReloadStrategy()

@property (nonatomic, assign) BOOL loaded;

@end

@implementation BMFOnlyOnceOKReloadStrategy

- (void) handleEvent:(BMFReloadStrategyEvent)event {
	[super handleEvent:event];
	
	if (!self.loaded) [self reload];
	
	[self startTimer];
}

- (void) loaded:(BOOL)success {
	[super loaded:success];

	if (success) self.loaded = YES;
}

@end

//
//  BMFResponderChainBehavior.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 09/07/14.
//
//

#import "BMFViewControllerBehavior.h"

#import "BMFTypes.h"

@interface BMFResponderChainBehavior : BMFViewControllerBehavior

@property (nonatomic, strong) NSArray *responders;

@property (nonatomic, assign) UIReturnKeyType lastResponderReturnKeyType;

@property (nonatomic, copy) BMFActionBlock lastResponderActionBlock;

- (instancetype) initWithResponders: (NSArray *) responders;

@end

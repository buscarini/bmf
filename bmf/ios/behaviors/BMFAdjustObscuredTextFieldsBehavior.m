//
//  BMFAdjustObscuredTextFieldsBehavior.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 07/07/14.
//
//

#import "BMFAdjustObscuredTextFieldsBehavior.h"

#import "BMF.h"
#import "BMFKeyboardManager.h"

#import <ReactiveCocoa/RACEXTScope.h>

static const CGFloat MINIMUM_SCROLL_FRACTION = 0.1;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;

@interface BMFAdjustObscuredTextFieldsBehavior() <UITextFieldDelegate>

@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) BOOL isChangingField;
@property (nonatomic, assign) UITextField *selectedTextField;
@property (nonatomic, assign) BOOL active;

@end

@implementation BMFAdjustObscuredTextFieldsBehavior

- (instancetype) initWithTextFields:(NSArray *) textFields {
	BMFAssertReturnNil(textFields.count>0);
	
    self = [super init];
    if (self) {
        _textFields = textFields;
		
		[self prepareTextFields];
		
		@weakify(self);
		[RACObserve([BMFBase sharedInstance].keyboardManager,keyboardHeight) subscribeNext:^(NSNumber *visible) {
			@strongify(self);
			if (!self.active) return;
			if (visible.boolValue) {
				[self adjust];
			}
			else {
				[self removeAdjustments];
			}
		}];
		
    }
    return self;
}

- (instancetype)init {
	BMFInvalidInit(initWithTextFields:);
}

- (void) setObject:(UIViewController<BMFBehaviorsViewControllerProtocol> *)object {
	[super setObject:object];
	
	self.origin = object.view.frame.origin;
	
	[self prepareTextFields];
}

- (void) setTextFields:(NSArray *)textFields {
	BMFAssertReturn(textFields.count>0);

	_textFields = textFields;
	
	[self prepareTextFields];
}

- (void) viewWillAppear:(BOOL)animated {
	self.active = YES;
}

- (void) viewWillDisappear:(BOOL)animated {
	self.active = NO;
}

- (void) prepareTextFields {
	if (!self.object) return;
	
	for (UITextField *tf in _textFields) {
		tf.delegate = (id)self.object.BMF_proxy;
	}
}

- (void) adjust {
	UIViewController *viewController = [self viewControllerToAdjust];
	
	CGRect textFieldRect = [viewController.view.window convertRect:self.selectedTextField.bounds fromView:self.selectedTextField];
    CGRect viewRect = [viewController.view.window convertRect:viewController.view.bounds fromView:viewController.view];
	CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
	CGFloat numerator =	midline - viewRect.origin.y	- MINIMUM_SCROLL_FRACTION * viewRect.size.height;
	CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
	CGFloat heightFraction = numerator / denominator;
	
	if (heightFraction < 0.0) {
		heightFraction = 0.0;
	}
	else if (heightFraction > 1.0) {
		heightFraction = 1.0;
	}
	
	BMFKeyboardManager *keyboardManager = [BMFBase sharedInstance].keyboardManager;
	CGFloat animatedDistance = keyboardManager.keyboardHeight*heightFraction;
		
	CGRect viewFrame = viewController.view.frame;
//	if (UIInterfaceOrientationIsLandscape(self.object.interfaceOrientation)) {
//		viewFrame.origin.x += animatedDistance;
//	}
	viewFrame.origin.y -= animatedDistance;
    
	[UIView animateWithDuration:keyboardManager.animationDuration delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
		viewController.view.frame = viewFrame;
	} completion:nil];
}


- (void) removeAdjustments {
	if (self.isChangingField) return;

	
	UIViewController *viewController = [self viewControllerToAdjust];
	
	CGRect frame = viewController.view.frame;
	frame.origin = self.origin;
	viewController.view.frame = frame;
}

- (UIViewController *) viewControllerToAdjust {
	if (!self.object) return nil;
	
	BMFAssertReturnNil([self.object isKindOfClass:[UIViewController class]]);
	
	UIViewController *vc = self.object;
	while (vc.parentViewController) {
		vc = vc.parentViewController;
	};
	
	return vc;
}

#pragma mark UITextFieldDelegate

- (void) textFieldDidBeginEditing:(UITextField *)textField {
	self.isChangingField = YES;
	self.selectedTextField = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
	if (self.selectedTextField==textField) self.selectedTextField = nil;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
	self.isChangingField = NO;
	return YES;
}

@end

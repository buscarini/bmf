//
//  BMFResponderChainBehavior.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 09/07/14.
//
//

#import "BMFResponderChainBehavior.h"

#import "BMF.h"

@interface BMFResponderChainBehavior() <UITextFieldDelegate>

@end

@implementation BMFResponderChainBehavior

- (instancetype) initWithResponders: (NSArray *) responders {
	BMFAssertReturnNil(responders.count>0);
	BMFAssertReturnNil([responders[0] isKindOfClass:[UIResponder class]]);
	
    self = [super init];
    if (self) {
        _responders = responders;
		_lastResponderReturnKeyType = UIReturnKeyDefault;

		[self updateResponders];
    }
    return self;
}

- (instancetype)init {
	BMFInvalidInit(initWithResponders:);
}

- (void) setResponders:(NSArray *)responders {
	BMFAssertReturn(responders.count>0);
	BMFAssertReturn([responders[0] isKindOfClass:[UIResponder class]]);

	_responders = responders;
	[self updateResponders];
}

- (void) setObject:(UIViewController<BMFBehaviorsViewControllerProtocol> *)object{
	[super setObject:object];
	
	[self updateResponders];
}

- (void) updateResponders {
	[self.responders enumerateObjectsUsingBlock:^(UIResponder *responder, NSUInteger idx, BOOL *stop) {
		
		UITextField *textField = [UITextField BMF_cast:responder];
		if (textField) {
			textField.delegate = (id)self.object.BMF_proxy;
			
			if (idx==self.responders.count-1) {
				// Last responder
				textField.returnKeyType = _lastResponderReturnKeyType;
			}
			else {
				textField.returnKeyType = UIReturnKeyNext;
			}
		}
	}];
}

#pragma mark UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
	
	NSUInteger index = [self.responders indexOfObject:textField];
	
	if (index==self.responders.count-1) {
		if (self.lastResponderActionBlock) self.lastResponderActionBlock(self);
	}
	else {
		UIResponder *nextResponder = self.responders[index+1];
		[nextResponder becomeFirstResponder];
	}
	
	return YES;
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField {
	return YES;
}

@end

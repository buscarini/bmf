//
//  BMFLoadTaskBehavior.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 11/11/14.
//
//

#import "BMFViewControllerBehavior.h"

#import "BMFTypes.h"

#import "BMFTaskProtocol.h"
#import "BMFLoaderViewProtocol.h"
#import "BMFReloadStrategyProtocol.h"

typedef id<BMFTaskProtocol>(^BMFCreateTaskBlock)();

@interface BMFRunTaskBehavior : BMFViewControllerBehavior

@property (nonatomic, copy) BMFCreateTaskBlock createTaskBlock;
@property (nonatomic, copy) BMFCompletionBlock completionBlock;
@property (nonatomic, strong) id<BMFLoaderViewProtocol> loader;

/// BMFOnlyOnceOKReloadStrategy by default
@property (nonatomic, strong) id<BMFReloadStrategyProtocol> reloadStrategy;

- (instancetype) initWithCreateTaskBlock:(BMFCreateTaskBlock) createTaskBlock completionBlock:(BMFCompletionBlock) completionBlock loader:(id<BMFLoaderViewProtocol>)loader;

- (void) reload;

@end

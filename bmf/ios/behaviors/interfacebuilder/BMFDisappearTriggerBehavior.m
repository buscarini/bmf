//
//  BMFDisappearTriggerBehavior.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 12/11/14.
//
//

#import "BMFDisappearTriggerBehavior.h"

@implementation BMFDisappearTriggerBehavior

- (void) viewWillDisappear:(BOOL)animated {
	if (!self.isEnabled) return;
	
	[self performSelector:@selector(p_sendEvent) withObject:self afterDelay:self.delay];
}

- (void) p_sendEvent {
	[self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void) dealloc {
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
}

@end

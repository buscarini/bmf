//
//  BMFAdjustScrollPageBehavior.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 22/1/15.
//
//

#import "BMFAdjustScrollPageBehavior.h"

#import "BMF.h"

@interface BMFAdjustScrollPageBehavior()

@end

@implementation BMFAdjustScrollPageBehavior {
	NSInteger horizontalPageIndex;
	NSInteger verticalPageIndex;
}

- (void) viewDidLoad {
	BMFAssertReturn(self.scrollView.pagingEnabled);
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[self savePageIndexes];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self restorePageIndexes];
}

- (void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
	[self savePageIndexes];
	
	[coordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
		[self restorePageIndexes];
	}];
}

- (void) savePageIndexes {
	horizontalPageIndex = self.scrollView.contentOffset.x/self.scrollView.bounds.size.width;
	verticalPageIndex = self.scrollView.contentOffset.y/self.scrollView.bounds.size.height;
}

- (void) restorePageIndexes {
	CGPoint offset = CGPointZero;
	offset.x = self.scrollView.bounds.size.width*horizontalPageIndex;
	offset.y = self.scrollView.bounds.size.height*verticalPageIndex;

	[self.scrollView setContentOffset:offset animated:YES];
}


@end

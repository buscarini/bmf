//
//  BMFAppearTriggerBehavior.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 12/11/14.
//
//

#import "BMFAppearTriggerBehavior.h"

#import "BMFViewController.h"

@implementation BMFAppearTriggerBehavior

- (void) performInit {
	[super performInit];
	self.triggerForward = YES;
	self.triggerBackward = YES;
}

- (void) viewWillAppear:(BOOL)animated {
	if (!self.isEnabled) return;

	if (self.beforeAppear) {
		[self queueEvent];
	}
}

- (void) viewDidAppear:(BOOL)animated {
	if (!self.isEnabled) return;
	
	if (!self.beforeAppear) 	[self queueEvent];
}

- (void) queueEvent {
	BMFViewController *vc = [BMFViewController BMF_cast:self.object];
	if (!vc) {
		[self performSelector:@selector(p_sendEvent) withObject:self afterDelay:self.delay];
	}
	else {
		if ( (self.triggerForward && vc.navigationDirection==BMFViewControllerNavigationDirectionForward) ||
			(self.triggerBackward && vc.navigationDirection==BMFViewControllerNavigationDirectionBackward) ) {
			[self performSelector:@selector(p_sendEvent) withObject:self afterDelay:self.delay];
		}
	}
}

- (void) p_sendEvent {
	[self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void) dealloc {
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
}

@end

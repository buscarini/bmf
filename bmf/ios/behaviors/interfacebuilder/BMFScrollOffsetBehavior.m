//
//  BMFScrollOffsetBehavior.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 25/11/14.
//
//

#import "BMFScrollOffsetBehavior.h"

#import "BMF.h"
#import <ReactiveCocoa/RACEXTScope.h>

@interface BMFScrollOffsetBehavior()

@property (nonatomic, assign) CGPoint offset;

@end

@implementation BMFScrollOffsetBehavior

- (void) awakeFromNib {
	@weakify(self);
	RAC(self,offset) =	[RACObserve([BMFBase sharedInstance].keyboardManager, keyboardVisible) map:^id(NSNumber *visible) {
		@strongify(self);
		return [NSValue valueWithCGPoint:self.scrollView.contentOffset];
	}];
}

- (void) setScrollView:(UIScrollView *)scrollView {
	_scrollView = scrollView;
	_scrollView.delegate = self;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	self.offset = self.scrollView.contentOffset;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	self.offset = self.scrollView.contentOffset;
}

#pragma mark UIScrollViewDelegate

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
	CGFloat inc = fabsf(scrollView.contentOffset.y-self.offset.y);
	if (inc>self.minimumOffset) {
		[self sendActionsForControlEvents:UIControlEventValueChanged];
	}
}

@end


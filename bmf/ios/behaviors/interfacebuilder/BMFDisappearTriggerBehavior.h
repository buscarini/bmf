//
//  BMFDisappearTriggerBehavior.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 12/11/14.
//
//

#import "BMFViewControllerBehavior.h"

@interface BMFDisappearTriggerBehavior : BMFViewControllerBehavior

/// Do this only the first time the view appears or always
@property (nonatomic, assign) IBInspectable BOOL onlyOnce;
@property (nonatomic, assign) IBInspectable CGFloat delay;

@end

//
//  BMFRunAlphaAnimationBehavior.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 12/11/14.
//
//

#import "BMFRunAnimationBehavior.h"

IB_DESIGNABLE
@interface BMFAlphaAnimationBehavior : BMFRunAnimationBehavior <BMFViewsAnimationBehaviorProtocol>

@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *views;

@property (nonatomic, assign) IBInspectable CGFloat initialValue;
@property (nonatomic, assign) IBInspectable CGFloat finalValue;

@end

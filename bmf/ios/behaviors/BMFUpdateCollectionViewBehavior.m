//
//  BMFUpdateCollectionViewBehavior.m
//  BMF
//
//  Created by Jose Manuel Sánchez Peñarroja on 14/03/14.
//  Copyright (c) 2014 José Manuel Sánchez. All rights reserved.
//

#import "BMFUpdateCollectionViewBehavior.h"

#import "BMFTypes.h"

@implementation BMFUpdateCollectionViewBehavior {
	id appActiveObserver;
}


- (instancetype) initWithView:(UICollectionView *) collectionView {
	BMFAssertReturnNil(collectionView);
	
    self = [super init];
    if (self) {
        self.collectionView = collectionView;
    }
    return self;
}

- (instancetype)init {
	[NSException raise:@"collectionView is required" format:@"use initWithView: instead"];
	return nil;
}

- (void) viewWillAppear:(BOOL)animated {
	// Update layout without animation
	[self updateLayoutAnimated:NO];
}

- (void) viewDidAppear:(BOOL)animated {
	appActiveObserver = [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
		[self updateLayoutAnimated:NO];
	}];
}

- (void) viewDidDisappear:(BOOL)animated {
	[self stopObserving];
}

- (void) dealloc {
	[self stopObserving];
}

- (void) stopObserving {
	[[NSNotificationCenter defaultCenter] removeObserver:appActiveObserver], appActiveObserver = nil;
}

- (void) viewDidLayoutSubviews {
	[self updateLayoutAnimated:NO];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self updateLayoutAnimated:YES];
}

- (void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
	[coordinator animateAlongsideTransition:^(id <UIViewControllerTransitionCoordinatorContext>context){
		[self updateLayoutAnimated:YES];
	} completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
	
	}];
}

- (void) updateLayoutAnimated:(BOOL) animated {
	DDLogDebug(@"update layout animated: %d",animated);

	if (animated) {
		[self.collectionView performBatchUpdates:nil completion:^(BOOL finished){
			if (finished) {
				[self.collectionView reloadData];
			}

		}];
	}
	else {
		[self.collectionView reloadData];
	}
}

@end

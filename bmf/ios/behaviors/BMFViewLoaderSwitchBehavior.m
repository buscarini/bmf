//
//  BMFViewLoaderSwitchBehavior.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 15/07/14.
//
//

#import "BMFViewLoaderSwitchBehavior.h"

#import "BMF.h"

@implementation BMFViewLoaderSwitchBehavior

- (instancetype) initWithLoaderView:(UIView<BMFLoaderViewProtocol> *) loaderView view:(UIView *)view {
	BMFAssertReturnNil(loaderView);
	BMFAssertReturnNil(view);
	
    self = [super init];
    if (self) {
        _loaderView = loaderView;
		_view = view;
		
		RACSignal *runningSignal = RACObserve(self, loaderView.progress.running);
		
		RAC(self.loaderView,hidden) = [runningSignal not];
		RAC(self.view,hidden) = runningSignal;
    }
    return self;
}

- (instancetype) init {
	BMFInvalidInit(initWithLoaderView:view:);
}

- (void) setLoaderView:(UIView<BMFLoaderViewProtocol> *)loaderView {
	BMFAssertReturn(loaderView);
	_loaderView = loaderView;
}

- (void) setView:(UIView *)view {
	BMFAssertReturn(view);
	_view = view;
}



@end

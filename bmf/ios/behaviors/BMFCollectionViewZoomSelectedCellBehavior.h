//
//  BMFCollectionViewZoomSelectedCellBehavior.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 21/07/14.
//
//

#import "BMFItemTapBehavior.h"

#import "BMFObjectControllerProtocol.h"

@interface BMFCollectionViewZoomSelectedCellBehavior : BMFItemTapBehavior <BMFObjectControllerProtocol>

@property (nonatomic, strong) BMFObjectDataStore *objectStore;

@end

//
//  BMFHideEmptyViewBehavior.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 07/07/14.
//
//

#import "BMFHideEmptyViewBehavior.h"

#import "BMF.h"

#import <ReactiveCocoa/RACEXTScope.h>

@implementation BMFHideEmptyViewBehavior {
	RACDisposable *disposable;
}

- (instancetype) initWithView:(UIView *) view {
	BMFAssertReturnNil(view);
	BMFAssertReturnNil([view isKindOfClass:[UITableView class]] || [view isKindOfClass:[UICollectionView class]]);
	
    self = [super init];
    if (self) {
        _view = view;
		
		@weakify(self);
		[RACObserve(self, view) subscribeNext:^(UIView *view) {
			@strongify(self);
			[disposable dispose], disposable = nil;
			disposable = [[view rac_signalForSelector:@selector(reloadData)] subscribeNext:^(id x) {
				[self check];
			}];
		}];
    }
    return self;
}

- (instancetype)init {
	BMFInvalidInit(initWithView:);
}

- (void) setView:(UIView *)view {
	BMFAssertReturn(view);
	
	_view = view;
}

- (void) viewWillAppear:(BOOL)animated {
	[self check];
}

- (void) check {
	BOOL empty = NO;
	UITableView *tableView = [UITableView BMF_cast:self.view];
	if (tableView) {
		empty = [tableView BMF_isEmpty];
	}
	else {
		UICollectionView *collectionView = [UICollectionView BMF_cast:self.view];
		if (collectionView) {
			empty = [collectionView BMF_isEmpty];
		}
	}
	
	_view.hidden = empty;
	self.placeholderView.hidden = !empty;
}

@end

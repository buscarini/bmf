//
//  BMFItemTapBehavior.m
//  BMF
//
//  Created by Jose Manuel Sánchez Peñarroja on 20/03/14.
//  Copyright (c) 2014 José Manuel Sánchez. All rights reserved.
//

#import "BMFItemTapBehavior.h"

#import "BMFDataSourceProtocol.h"
#import "BMFDataReadProtocol.h"

#import "BMF.h"

@implementation BMFItemTapBehavior

- (instancetype)init {
	self = [super init];
	if (self) {
		self.deselectItemOnTap = YES;
    }
	return self;
}

- (id) itemInDataSource:(id<BMFDataSourceProtocol>) dataSource atIndexPath:(NSIndexPath *) indexPath containerView:(UIView *) containerView {
	BMFAssertReturnNil([dataSource conformsToProtocol:@protocol(BMFDataSourceProtocol)]);
	
	id<BMFDataReadProtocol> dataStore = dataSource.dataStore;
	return [dataStore itemAt:indexPath];
}

#pragma mark UITableViewDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (self.deselectItemOnTap) [tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self tapInDataSource:(id<BMFDataSourceProtocol>)tableView.dataSource indexPath:indexPath containerView:tableView];
}

- (void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	id item = [self itemInDataSource:(id<BMFDataSourceProtocol>)tableView.dataSource atIndexPath:indexPath containerView:tableView];

	[self accessoryItemTapped:item atIndexPath:indexPath containerView:tableView];
}

#pragma mark UICollectionViewDelegate

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	if (self.deselectItemOnTap) [collectionView deselectItemAtIndexPath:indexPath animated:YES];
	[self tapInDataSource:(id<BMFDataSourceProtocol>)collectionView.dataSource indexPath:indexPath containerView:collectionView];
}

#pragma mark Item tap

- (void) tapInDataSource:(id<BMFDataSourceProtocol>) dataSource indexPath:(NSIndexPath *)indexPath containerView:(UIView *) containerView {
	id item = [self itemInDataSource:dataSource atIndexPath:indexPath containerView:containerView];
	[self itemTapped:item atIndexPath:indexPath containerView:containerView];
}

- (void) itemTapped:(id) item atIndexPath:(NSIndexPath *)indexPath containerView:(UIView *)containerView {
	BMFAbstractMethod();
}

- (void) accessoryItemTapped:(id) item atIndexPath:(NSIndexPath *) indexPath containerView:(UIView *) containerView {}

@end

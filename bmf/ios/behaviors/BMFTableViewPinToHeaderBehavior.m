//
//  BMFTableViewPinToHeaderBehavior.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 20/06/14.
//
//

#import "BMFTableViewPinToHeaderBehavior.h"

#import "BMF.h"

@implementation BMFTableViewPinToHeaderBehavior

- (instancetype) initWithView:(UITableView *)tableView {
    self = [super init];
    if (self) {
		_tableView = tableView;
	}
    return self;
}

- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
	BMFAssertReturn(self.tableView.tableHeaderView);
	
//	CGFloat computedTargetOffset = scrollView.contentOffset.y*velocity.y+self.tableView.contentInset.top;
	CGFloat computedTargetOffset = scrollView.contentOffset.y+self.tableView.contentInset.top;
	
	//CGFloat time = fabsf(targetContentOffset->y-scrollView.contentOffset.y)/fabsf(velocity.y);
	CGFloat time = 0.2;
	
//	if (computedTargetOffset>self.tableView.tableHeaderView.bounds.size.height) return;
	DDLogInfo(@"Animation time: %f",time);
	
	[UIView animateWithDuration:time delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//	[UIView animateWithDuration:time animations:^{
		//UIEdgeInsets oldInsets = self.tableView.contentInset;
		
		if (computedTargetOffset<(self.tableView.tableHeaderView.bounds.size.height/3.0)) {
			*targetContentOffset = CGPointMake(targetContentOffset->x, 0);
			self.tableView.contentInset = UIEdgeInsetsZero;
			
			//		CGPoint offset = self.tableView.contentOffset;
			//		offset.y += oldInsets.top;
			//		self.tableView.contentOffset = offset;
		}
		else {
			if (computedTargetOffset<(self.tableView.tableHeaderView.bounds.size.height)) {
				*targetContentOffset = CGPointMake(targetContentOffset->x, self.tableView.tableHeaderView.bounds.size.height);
			}
			
			self.tableView.contentInset = UIEdgeInsetsMake(-self.tableView.tableHeaderView.bounds.size.height, 0, 0, 0);
		}
	} completion:nil];
	
	
//	CGPoint offset = self.tableView.contentOffset;
//	offset.y += (self.tableView.contentInset.top-oldInsets.top);
//	self.tableView.contentOffset = offset;

}


@end

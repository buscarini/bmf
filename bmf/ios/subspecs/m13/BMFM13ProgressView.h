//
//  BMFM13ProgressView.h
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/05/14.
//
//

#import <Foundation/Foundation.h>

#import "BMFProgressUI.h"

@class M13ProgressView;

@interface BMFM13ProgressView : BMFProgressUI

- (instancetype) initWithView:(M13ProgressView *) progressView;

@end

//
//  BMFM13ProgressView.m
//  Pods
//
//  Created by Jose Manuel Sánchez Peñarroja on 27/05/14.
//
//

#import "BMFM13ProgressView.h"

#import "BMF.h"
#import <M13ProgressSuite/M13ProgressView.h>
#import <ReactiveCocoa/RACEXTScope.h>

@interface BMFM13ProgressView()

@property (nonatomic, strong) M13ProgressView *progressView;

@end

@implementation BMFM13ProgressView

- (instancetype) initWithView:(M13ProgressView *) progressView {
    self = [super init];
    if (self) {
        self.progressView = progressView;
	}
    return self;
}

- (instancetype)init {
	BMFInvalidInit(initWithView:);
}

- (void) addToViewController:(UIViewController *) vc {
	[vc.view addSubview:self.progressView];
	[BMFAutoLayoutUtils centerView:self.progressView inParent:vc.view];
}

- (void) updateRunning: (BOOL) running {
	if (running) {
		[self.progressView setProgress:self.progress.fractionCompleted animated:NO];
		self.progressView.alpha = 1;
	}
	else {
		if (self.progress.fractionCompleted==1) {
			if (self.progress.failedError) {
				[self.progressView performAction:M13ProgressViewActionFailure animated:YES];
			}
			else {
				[self.progressView performAction:M13ProgressViewActionSuccess animated:YES];
			}
		}
	}
}

- (void) updateProgress:(CGFloat) progress {
	[self.progressView setProgress:progress animated:YES];
}

@end
